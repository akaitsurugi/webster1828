# Required Elements
[Webster1828]
Abbreviation=Webster 1828
Description=Webster's Dictionary 1828
DataPath=./modules/lexdict/zld/webster1828/webster1828
ModDrv=zLD

# Required Elements with defaults
SourceType=TEI
Encoding=UTF-8
CompressType=ZIP

# General informatic and installer elements
About=The purpose for creating this is to preserve, and promote the use of, the 1828 version of Webster's Dictionary. After reviewing several dictionaries, I found this one to have the best definitions, especially for understanding things written during the 17th-19th centuries. Dictionaries that came out in the 20th and 21st centuries are lackluster compared to the previous ones, since post-modernists felt that language was another "social construct" that needed to be torn apart and turned into nothing. You can clearly see this by comparing definitions over time, where they become more vague and sometimes even the opposite of what they were originally. This is maintained by Akai Tsurugi.
SwordVersionDate=2023-05-24
Version=1.1
History_1.0=Initial release
History_1.1=Updated for Akai Tsurugi, fixed definitions
MinimumVersion=1.5.11
Lang=en

# Copyright & Licensing related elements
Copyright=CC BY-SA 4.0: This license enables reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use. If you remix, adapt, or build upon the material, you must license the modified material under identical terms. \par\parAkai Tsurugi website: akaitsurugi.org \parGit repo: codeberg.org/akaitsurugi/webster1828 \parCC BY-SA 4.0 information: creativecommons.org/licenses/by-sa/4.0
DistributionLicense=Creative Commons: BY-SA 4.0
TextSource=http://codeberg.org/akaitsurugi/webster1828/webster1828.csv
LCSH=English language--Dictionaries.
